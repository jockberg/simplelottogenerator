import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.DirectoryStream.Filter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.filechooser.FileSystemView;

public class Lotto {

	List<String> oldRows, newRows;
	Random rand = new Random();
	
	
	public void loadOld() {
		newRows = new ArrayList<>();
		String fileName = "https://spelstatistiken.se/PHP/lotto12/lottord.txt"; 
		InputStream is;
		try {
			is = new URL(fileName).openConnection().getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			oldRows = br.lines()
					.filter(e-> !e.isEmpty() && e.contains("  "))
					.map(e-> e=e.substring(e.indexOf(">")+1, e.indexOf("  ")))
					.collect(Collectors.toList());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public String getRandomRow() {
		String tempRow=",";
		for(int i=0;i<7;i++) {
			int next = rand.nextInt(35)+1;
			while(tempRow.contains(","+next+",") && tempRow.length()!=1) {
				next=rand.nextInt(35)+1;
			}
			tempRow+=next+",";
		}
		return tempRow;
	}

	public void generateNew(int NumberOfrows) {

		while(newRows.size()!=NumberOfrows) {
			String tempRow=getRandomRow();
			if(tempRow.contains(",5,") && tempRow.contains(",16,") && tempRow.contains(",2,") && tempRow.contains(",3,")) {
				tempRow=tempRow.substring(1,tempRow.length()-1);
				if(!oldRows.contains(tempRow) && !newRows.contains(tempRow)) {
					newRows.add(tempRow);
				}
			}
		}
	}

	public void writeToFile() {
		try {
			FileSystemView filesys = FileSystemView.getFileSystemView();
			Files.write(Paths.get(filesys.getHomeDirectory()+"//lottoRader.txt"), newRows);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		Lotto l = new Lotto();
		l.loadOld();
		l.generateNew(30);
		l.writeToFile();
	}
}